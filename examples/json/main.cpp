#include <tini/formats/json/load.hpp>

#include <nlohmann/json.hpp>

#include <iostream>

using namespace nlohmann::literals;

std::string Data() {
  auto data = R"(
  {
    "name": "example",
    "params": {
      "init": 42,
      "max": 11,
      "atomic": true,
      "timeout.ms": 100,
      "port": 123
    }
  })"_json;

  return data.dump();
}

int main() {
  auto cfg = tini::formats::json::Load(Data());

  assert(cfg.Has("name"));
  std::cout << cfg.GetString("name") << std::endl;

  {
    auto params = cfg.SubSection("params");

    std::cout << params.GetInt64("init") << std::endl;
    std::cout << params.GetUInt64("max") << std::endl;
    std::cout << params.GetUInt16("port") << std::endl;
    std::cout << params.GetFlag("atomic") << std::endl;
    std::cout << params.GetMillis("timeout").count() << std::endl;
  }

  return 0;
}

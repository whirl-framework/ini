#pragma once

#include <string_view>

namespace tini {

using Key = std::string_view;

}  // namespace tini
#include <tini/config.hpp>

#include <cassert>
#include <numeric>

#include <sstream>

namespace tini {

uint16_t Config::GetUInt16(Key key) const {
  const uint64_t value = GetUInt64(key);
  assert(value <= std::numeric_limits<uint16_t>::max());
  return (uint16_t)value;
}

std::chrono::milliseconds Config::GetMillis(Key key) const {
  std::stringstream ms_key;
  ms_key << key << ".ms";

  const uint64_t millis = GetUInt64(ms_key.str());

  return std::chrono::milliseconds(millis);
}

}  // namespace tini

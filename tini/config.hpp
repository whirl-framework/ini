#pragma once

#include <tini/section.hpp>

namespace tini {

class Config {
 public:
  bool Has(Key key) const {
    return impl_->Has(key);
  }

  int64_t GetInt64(Key key) const {
    return impl_->GetInt64(key);
  }

  uint64_t GetUInt64(Key key) const {
    return impl_->GetUInt64(key);
  }

  // Port numbers
  uint16_t GetUInt16(Key key) const;

  bool GetFlag(Key key) const {
    return impl_->GetFlag(key);
  }

  std::string GetString(Key key) const {
    return impl_->GetString(key);
  }

  // Timeouts
  std::chrono::milliseconds GetMillis(Key key) const;

  Config SubSection(Key key) {
    return From(impl_->GetSubSection(key));
  }

  static Config From(ISectionPtr impl) {
    return Config(std::move(impl));
  }

 private:
  Config(ISectionPtr impl)
    : impl_(std::move(impl)) {
  }

 private:
  ISectionPtr impl_;
};

}  // namespace tini

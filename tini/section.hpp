#pragma once

#include <tini/key.hpp>

#include <string>
#include <cstdlib>
#include <chrono>

namespace tini {

//////////////////////////////////////////////////////////////////////

struct ISection;

using ISectionPtr = std::shared_ptr<ISection>;

//////////////////////////////////////////////////////////////////////

struct ISection {
  virtual ~ISection() = default;

  virtual bool Has(Key key) const = 0;

  // Access fields

  // TODO: Replace by single Field Get(key key)?

  virtual int64_t GetInt64(Key key) const = 0;
  virtual uint64_t GetUInt64(Key key) const = 0;

  virtual bool GetFlag(Key key) const = 0;

  virtual std::string GetString(Key key) const = 0;

  // Subsections

  virtual ISectionPtr GetSubSection(Key key) const = 0;

  // Dump section to string
  virtual std::string Dump() const = 0;
};

}  // namespace tini

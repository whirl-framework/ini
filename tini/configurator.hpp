#pragma once

#include <tini/config.hpp>

namespace tini {

struct IConfigurator {
  virtual ~IConfigurator() = default;

  virtual Config Access() = 0;
};

}  // namespace tini

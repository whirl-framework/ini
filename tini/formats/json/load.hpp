#pragma once

#include <tini/config.hpp>

namespace tini {

namespace formats::json {

Config Load(std::string json);

}  // namespace formats::json

}  // namespace tini

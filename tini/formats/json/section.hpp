#pragma once

#include <tini/section.hpp>

#include <nlohmann/json.hpp>

namespace tini {

namespace formats::json {

class Object : public ISection {
 public:
  Object(nlohmann::json object)
    : object_(std::move(object)) {
  }

  bool Has(Key key) const override {
   return object_.contains(key);
  }

  int64_t GetInt64(Key key) const override {
    return object_[key].get<int64_t>();
  }

  uint64_t GetUInt64(Key key) const override {
    return object_[key].get<uint64_t>();
  }

  bool GetFlag(Key key) const override {
    return object_[key].get<bool>();
  }

  std::string GetString(Key key) const override {
    return object_[key].get<std::string>();
  }

  ISectionPtr GetSubSection(Key key) const override {
    return std::make_shared<Object>(object_[key]);
  }

  std::string Dump() const override {
    return object_.dump();
  }

 private:
  nlohmann::json object_;
};

}  // namespace formats::json

}  // namespace tini

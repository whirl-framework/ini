#include <tini/formats/json/load.hpp>

#include <tini/formats/json/section.hpp>

namespace tini {

namespace formats::json {

Config Load(std::string json) {
  auto json_object = nlohmann::json::parse(json);
  auto root = std::make_shared<Object>(std::move(json_object));
  return Config::From(root);
}

}  // namespace formats::json

}  // namespace tini

cmake_minimum_required(VERSION 3.14)

project(tini)

option(TINI_TESTS "Enable tini tests" OFF)
option(TINI_DEVELOPER "Tini developer mode" OFF)

add_subdirectory(third_party)

include(cmake/CompileOptions.cmake)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "bin")

add_library(tini
        tini/section.hpp
        tini/config.hpp
        tini/config.cpp
        tini/formats/json/section.hpp
        tini/formats/json/section.cpp
        tini/formats/json/load.cpp
        tini/formats/json/load.hpp
        )

target_link_libraries(tini PUBLIC
        nlohmann_json::nlohmann_json)

target_include_directories(tini PUBLIC .)

if(TINI_TESTS OR TINI_DEVELOPER)
    add_subdirectory(examples)
endif()
